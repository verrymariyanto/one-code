import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Navbar from "../components/Navbar";
import "../styles/profile.css";

function Profile() {
  const profile = useSelector((state) => state.allReducers.user);

  useEffect(() => {
    // console.log(profile,"---profile");
  });
  return (
    <>
      <Navbar />
      <div className="profile">
        <h4>Username : {profile.name}</h4>
        <h4>Email : {profile.email}</h4>
        <h4>
          Address :{" "}
          {`${profile.address.street} ${profile.address.suite} ${profile.address.city} ${profile.address.zipcode}`}
        </h4>
        <h4>Phone : {profile.phone}</h4>
      </div>
    </>
  );
}

export default Profile;
