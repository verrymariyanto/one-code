import React from "react";
import { Button, Container, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "../styles/home.css";

function Home() {
  const navigate = useNavigate();
  const toLogin = (e) => {
    e.preventDefault();
    navigate("/login");
  };
  return (
    <div className="home">
      <Navbar>
        <Container>
          <Navbar.Brand>
            <h3>CINTA CODING</h3>
          </Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Button variant="primary" size="lg" onClick={toLogin}>
              Login
            </Button>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}

export default Home;
